from user import User
from database import DB

DB.initialize(database="test", user="h", password="1234", host="localhost")

DB.execQuery("CREATE TABLE IF NOT EXISTS users ("
             "id SERIAL NOT NULL PRIMARY KEY ,"
             "email VARCHAR(64) NOT NULL ,"
             "first_name VARCHAR(64) NOT NULL ,"
             "last_name VARCHAR(64));")

new_user = User("huyen@maidbot.com", "Huyen", "Vu", 1)
new_user.save_to_db()

print(User.get_user_by_email("huyen@maidbot.com"))

print(User.get_all())

User.flush_table()
