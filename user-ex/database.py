import psycopg2

from psycopg2 import pool


class DB:
    __connection_pool = None

    @staticmethod
    def initialize(**kwargs):
        DB.__connection_pool = pool.SimpleConnectionPool(1, 10, **kwargs)

    @staticmethod
    def getConnection():
        return DB.__connection_pool.getconn()


    @staticmethod
    def putConnection(conn):
        DB.__connection_pool.putconn(conn)


    @staticmethod
    def execQuery(query):
        conn = DB.__connection_pool.getconn()
        cursor = conn.cursor()
        res = cursor.execute(query)
        if res is not None:
            response = []
            for row in cursor:
                response.append(row)
            return response

class DBCursor:
    def __init__(self):
        self.connection = None
        self.con = None
        self.cursor = None

    def __enter__(self):
        self.connection = DB.getConnection()
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.commit()
        self.cursor.close()
        DB.putConnection(self.connection)

    def execQuery(self, query):
        cursor = self.con.cursor()
        try:
            res = cursor.execute(query)
            result = []
            if res is not None:
                for row in cursor:
                    result.append(row)
            self.con.commit()
            return result
        except ValueError:
            print("Can't execute query", ValueError)
        cursor.close()
