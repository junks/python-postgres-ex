import psycopg2
from database import DBCursor, DB

class User:
    def __init__(self, email, first_name, last_name, id):
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.id = id

    def save_to_db(self):
        try:
            with DBCursor() as cursor:
                cursor.execute("INSERT INTO users (email, first_name, last_name) VALUES (%s, %s, %s)", (self.email, self.first_name, self.last_name))
            print("saved to db")
        except ValueError:
            print("failed to connect to db", ValueError)

    def __repr__(self):
        return "<User {}>".format(self.email)

    @classmethod
    def get_user_by_email(cls, email):
        try:
            with DBCursor() as cursor:
                cursor.execute('SELECT * FROM users WHERE email=%s', (email,))
                user_data = cursor.fetchone()
                return cls(user_data[1], user_data[2], user_data[3], user_data[0])
        except ValueError:
            print(ValueError)

    @classmethod
    def flush_table(cls):
        try:
            with DBCursor() as cursor:
                cursor.execute('DELETE FROM users WHERE TRUE')
        except ValueError:
            print(ValueError)

    @classmethod
    def get_all(cls):
        try:
            res = DB.execQuery("SELECT * FROM users")
            return res
        except ValueError:
            print(ValueError)
